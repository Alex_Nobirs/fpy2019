# Отчет об изучении GNU/Linux(Ubuntu)

## Содержание

  * [Введение](##Введение)
  * [Ход работы](##Ход\ работы) 
    * [Установка системы](##Установка\ системы) 
    * [История появления Linux](#История\ появления\ Linux)
    * [Сфера применения](#Сфера\ применения)
    * [Отличительные черты](#Отличительные\ черты)
    * [Изучение командной строки](#Изучение\ командной\ строки)
  * [Результаты](#Результаты)
  * [Выводы](##Выводы)
  * [Литература](#Литература)

## Введение

В одном из заданий **task001** передо мной стояли задачи установить, изучить, а также, возможно, использовать Linux в качестве основной системы.  

## Ход работы

### Установка системы

Для начала мне было необходимо установить себе на компьютер один из 
дистрибутивов Linux. Мною был выбран Ubuntu, как один из самых 
распространенных и надежных дистрибутивов. После скачивания последней 
LTS версии была создана установочная флешка, с которой в последствии  
была установлена система. Ubuntu была установлена как вторая операционная система рядом с  Windows 10.

### История появления Linux

Unix начала разрабатыватся в 1969 г. , в середине 70-х получил широкое распространение. В 1993 году  был основан проект GNU. К концу лета 1991 г. появился сырой вариант ядра.

Так как проекту GNU до полноценной системы не хватало только ядра, Linux оказался как нельзя кстати. Ядро превратилось в полноценную операционную систему GNU/Linux. Чаще всего её называют просто Linux.

### Сфера применения

С течением времени Linux адаптировали под разные архитектуры. В настоящее время его можно встретить  на многих электронных устройствах. Android также создан на базе ядра Linux.

### Отличительные черты

В данном разделе речь пойдет о Linux  с точки зрения пользователя.
Сама операционная система весьма удобна, а также предоставляет большой спектр возможностей как для обычного(рядового) пользователя, так и для опытного программиста. Файловая система устроена в виде дерева, на вершине которого находится корневой какалог. В систему легко и просто устанавливать сторонние приложения. Причем делать это можно как обычными способами, так и напрямую через консоль(терминал).

### Изучение командной строки

Для изучения командной строки была использована книга Уильяма Шотса "Командная строка Linux". Практически все команды и примеры проверялись локально, я также эксперементировал с разными вариантами использования команд.

---

### Результаты

В результате был установлен Linux, а также изучены основы работы в командной строке Linux. Также мною было написано несколько простейших скриптов на bash для автоматизации и упрощения некоторых рутинных действий.

### Выводы

Linux -  широко распространенная и очень удобная операционная система как для обычных пользователей, так и для опытных программистов. Она предоставляет намного большую свободу действий по сравнению с большинством операционных систем, в особенности по сравнению с Windows. Данная система показалась мне весьма удобной и не сложной в изучении, а также она предоставляет широкий спектр возможностей пользователю, что делает её еще более удобной и привлекательной.
в дальнейшем я планирую использовать ее как основную операционную систему.

---

### Литература

В качестве учебной литературы использовалась [книга Уильяма Шотса "Командная строка Linux"](http://linuxcommand.org/tlcl.php)